//This class contains menu items from json file.
class menuItems {
    constructor (jsonmenu) {
        this._htmlMenu = new Map();
        for (const [name, items] of Object.entries(jsonmenu)) {
            const container = document.createElement('div');
            container.classList.add('rollMenu');
            for (let j = 0; j < items.length; j++) {
                const button = document.createElement('a');
                button.classList.add('rollItem');
                button.textContent = items[j];
                button.addEventListener("click", ()=>{
                    localStorage.setItem("menu_object", name);
                    localStorage.setItem("menu_order", j);
                })
                button.href = "page.html";
                container.appendChild(button);
            }
            this._htmlMenu.set(name, container);
        }
    }
    getMenuItems (){
        return this._menuItems;
    }
    getHtmlMenu(){
        return this._htmlMenu;
    }
}
//This function is called on load and everytime on window resize, it resize church image and facebook icon.
function resizer(reduction) {
    for (let i = 0; i < 5; i++) {
        const w = document.documentElement.clientWidth;
        document.getElementById("image-show").style.width = w+"px";
        document.getElementById("image-show").style.height = ((w/3.67333333)/reduction)+"px";
    }
    const fbEl = document.querySelector('#fb_redirect');
    fbEl.style.top = document.querySelector('header').clientHeight + "px";
}
//init function called on page load. It allows title icon be clickable, set up menu and page content from JSON files.
function init(){    
    var titulEl = document.querySelector('#titul')
    titulEl.addEventListener("click", ()=>{
        window.location.href = "index.html";
    });
    addPageListener("#svatosti");
    myRequest("pages.json")
    .then(e => {
        const data = e.target.responseText;
        const dataJson = JSON.parse(data);
        renderPage(dataJson);
    })
    .catch(e => {
        console.log(e);
    });
    myRequest("menu.json")
    .then(e => {
        const data = e.target.responseText;
        const jsonmenu = JSON.parse(data);
        addMenuListeners(jsonmenu);
    })
    .catch(e => {
        console.log(e);
    });       
}
//This function adds event Listener to all menu items in navigation bar. Function onClick roll up menu with specific content and offClick allows click whenever in document to close this menu.
function addMenuListeners(jsonmenu){         
    let items = document.getElementsByClassName('menu');
    for (let item of items) {
        item.addEventListener("click", function onClick(e){
            const mItems = new menuItems(jsonmenu);
            const pageEl = document.querySelector('.page')
            let container = mItems.getHtmlMenu().get(e.target.id);
            container.style.width = e.target.style.width;
            container.style.left = e.target.offsetLeft + "px";
            container.style.top = (e.target.offsetTop + e.target.offsetHeight) + "px";
            const childrens = container.children;
            for (let i = 0; i < childrens.length; i++) {
                childrens[i].style.width = e.target.clientWidth + "px";
                childrens[i].style.fontSize = getComputedStyle(e.target).fontSize;
            }
            const offClick = (evt) => {
                if(e != evt && evt.target.className != "rollItem"){
                    pageEl.removeChild(container);
                    document.removeEventListener('click', offClick);
                    for (let it of items) {
                        it.addEventListener('click', onClick);
                    }
                }
            }
            document.addEventListener('click', offClick);
            for (let it of items) {
                it.removeEventListener('click', onClick); 
            }
            pageEl.appendChild(container);
        });
    }
}
//This function renders page content depended on local storage variables.
function renderPage(dataJson){
    const name = localStorage.getItem("menu_object");
    const order = localStorage.getItem("menu_order");
    targetEl = document.querySelector('article');
    const h1El = document.createElement('h1');
    h1El.textContent = dataJson[name][order].nadpis;
    targetEl.appendChild(h1El);
    if(name == "farnost" && order == 4){
        targetEl.innerHTML += dataJson[name][order].obsah;
    }else{
        let pEl = document.createElement('p');
        const obsah = dataJson[name][order].obsah;
        for (let i = 0; i < obsah.length; i++) {
            if(obsah[i] == "\n"){
                targetEl.appendChild(pEl);
                pEl = document.createElement('p');
            }else{
                pEl.textContent+= obsah[i];
            }          
        }
        targetEl.appendChild(pEl);
    }
}
//This is universal function to handle XMLHttpRequests, it return Promise to deal with result.
function myRequest (url) {
    return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest();
        request.addEventListener("load", e => {
            resolve(e);
        });
        request.addEventListener("error", e => {
            reject(e);
        });
        request.overrideMimeType("application/json");
        request.open("GET", url);
        request.send();
    });
}
//Simple function for add listener to buttons in navigation without roll up menu.
function addPageListener(query){
    document.querySelector(query).addEventListener("click", ()=>{
        localStorage.setItem("menu_object", query.slice(1));
        localStorage.setItem("menu_order", 0);
    });      
}
init();