//This class contains menu items from json file.
class menuItems {
    constructor (jsonmenu) {
        this._htmlMenu = new Map();
        for (const [name, items] of Object.entries(jsonmenu)) {
            const container = document.createElement('div');
            container.classList.add('rollMenu');
            for (let j = 0; j < items.length; j++) {
                const button = document.createElement('a');
                button.classList.add('rollItem');
                button.textContent = items[j];
                button.addEventListener("click", ()=>{
                    localStorage.setItem("menu_object", name);
                    localStorage.setItem("menu_order", j);
                })
                button.href = "page.html";
                container.appendChild(button);
            }
            this._htmlMenu.set(name, container);
        }
    }
    getMenuItems (){
        return this._menuItems;
    }
    getHtmlMenu(){
        return this._htmlMenu;
    }
}
//This function is called on load and everytime on window resize, it resize church image and facebook icon.
function resizer(reduction) {
    for (let i = 0; i < 5; i++) {
        const w = document.documentElement.clientWidth;
        document.getElementById("image-show").style.width = w+"px";
        document.getElementById("image-show").style.height = ((w/3.67333333)/reduction)+"px";
    }
    const fbEl = document.querySelector('#fb_redirect');
    fbEl.style.top = document.querySelector('header').clientHeight + "px";
}
//init function called on page load. It allows title icon be clickable, set up menu from JSON file and add eventListeners to drop zone for Drag'n'Drop.
function init(){    
    var titulEl = document.querySelector('#titul')
    titulEl.addEventListener("click", ()=>{
        window.location.href = "index.html";
    });
    addPageListener("#svatosti");
    myRequest("menu.json")
    .then(e => {
        const data = e.target.responseText;
        const jsonmenu = JSON.parse(data);
        addMenuListeners(jsonmenu);
    })
    .catch(e => {
        console.log(e);
    });
    document.querySelector('#drop_zone').addEventListener("dragleave", changeBackground);
    document.querySelector('#drop_zone').addEventListener("mouseleave", changeBackground);
}
//This function adds event Listener to all menu items in navigation bar. Function onClick roll up menu with specific content and offClick allows click whenever in document to close this menu.
function addMenuListeners(jsonmenu){         
    let items = document.getElementsByClassName('menu');
    for (let item of items) {
        item.addEventListener("click", function onClick(e){
            const mItems = new menuItems(jsonmenu);
            const pageEl = document.querySelector('.page')
            let container = mItems.getHtmlMenu().get(e.target.id);
            container.style.width = e.target.style.width;
            container.style.left = e.target.offsetLeft + "px";
            container.style.top = (e.target.offsetTop + e.target.offsetHeight) + "px";
            const childrens = container.children;
            for (let i = 0; i < childrens.length; i++) {
                childrens[i].style.width = e.target.clientWidth + "px";
                childrens[i].style.fontSize = getComputedStyle(e.target).fontSize;
            }
            const offClick = (evt) => {
                if(e != evt && evt.target.className != "rollItem"){
                    pageEl.removeChild(container);
                    document.removeEventListener('click', offClick);
                    for (let it of items) {
                        it.addEventListener('click', onClick);
                    }
                }
            }
            document.addEventListener('click', offClick);
            for (let it of items) {
                it.removeEventListener('click', onClick); 
            }
            pageEl.appendChild(container);
        });
    }
}
//This is universal function to handle XMLHttpRequests, it return Promise to deal with result.
function myRequest (url) {
    return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest();
        request.addEventListener("load", e => {
            resolve(e);
        });
        request.addEventListener("error", e => {
            reject(e);
        });
        request.overrideMimeType("application/json");
        request.open("GET", url);
        request.send();
    });
}
//Simple function for add listener to buttons in navigation without roll up menu.
function addPageListener(query){
    document.querySelector(query).addEventListener("click", ()=>{
        localStorage.setItem("menu_object", query.slice(1));
        localStorage.setItem("menu_order", 0);
    });      
}
//This handler takes file when user drop it to drop zone, if file is not image, it is telled to user. Otherwise image is rendered to page and size in MB is shown as well.
function dropHandler(ev) {
  ev.preventDefault();
  if (ev.dataTransfer.items) {
    for (var i = 0; i < ev.dataTransfer.items.length; i++) {
      if (ev.dataTransfer.items[i].kind === 'file') {
        var file = ev.dataTransfer.items[i].getAsFile();
            if (!file.type.startsWith('image/')){
              document.querySelector('#drop_zone').innerHTML = "<p>Špatný formát souboru</p>";
              continue; 
            }
            const img_div = document.createElement("div");
            const img = document.createElement("img");
            const description = document.createElement("p");
            description.textContent = (file.size/1000000).toFixed(2) + " MB";
            img.file = file;
            const container = document.querySelector("#photo_container");
            img_div.appendChild(img);
            img_div.appendChild(description);
            container.appendChild(img_div);
            const reader = new FileReader();
            reader.onload = (function(aImg) { return function(e) {
                aImg.src = e.target.result;
            }; })(img);
            img.onload = (()=>{
                const nW = img.naturalWidth;
                console.log(nW);
                const nH = img.naturalHeight;
                console.log(nH);
                let divide_constant;
                if(nW > nH){
                    divide_constant = nW/200;
                }else{
                    divide_constant = nH/200;
                }
                console.log(divide_constant);
                img.style.height = nH/divide_constant + "px";
                img.style.width = nW/divide_constant + "px";
            });
        reader.readAsDataURL(file);
      } 
    }
  }
}
//This handler changes drop zone background and text, when user drag with items on zone.
function dragOverHandler(ev) {
    const dZ = document.querySelector('#drop_zone');
    dZ.style.background = "linear-gradient(63deg, #999 23%, transparent 23%) 7px 0,linear-gradient(63deg, transparent 74%, #999 78%),linear-gradient(63deg, transparent 34%, #999 38%, #999 58%, transparent 62%),#444";
    dZ.style.backgroundSize =  "16px 48px";
    dZ.innerHTML = "<p>Pusťte fotku zde</p>";
    ev.preventDefault();
}
//This handler changes drop zone background and text, when user drag out of drop zone.
function changeBackground(){
    const dZ = document.querySelector('#drop_zone');
    dZ.style.background = "";
    dZ.style.backgroundColor = "gray";
    dZ.innerHTML = "<p>Přetáhněte jednu nebo více fotek na toto pole</p>";
}
init();