# Webové stránky farnosti Semily
### Martin Lukeš
##### ČVUT FEL - B0B39KAJ

Cílem tohoto projektu bylo vytvoření webové aplikace v JavaScriptu pro předmět B0B39KAJ 
(Tvorba klientských aplikací v JavaScriptu). Mohli jsme si zvolit libovolné téma práce, a protože
jsem byl tázán, zda-li bych neudělal nové webové stránky pro semilskou farnost, a to i s administrací, 
která chyběla, tak jsem se rozhodl spojit tuto práci se svojí prací semestrální.

Nejdříve jsem si navrhnul design aplikace a pak jednotlivou funkcionalitu. Začal jsem s kostrou,
kde jsem hojně využíval utilitu flex box, která jak jsem zjistil později, má bugy v prohlížeči Edge.
Na ostatních popolárních prohlížečích, ale funguje bez problému. Současně s tím, jsem celou aplikaci 
dělal responsivní. Využil jsem k tomu media queries a javascriptové funkce (viz funkce resizer).
Dále jsem se snažil stránku zkrášlit css styly, přes XMLHttpRequest jsem importoval dva druhy kalendářů
a zprovoznil tlačítka na stránce. Nakonec jsem chtěl vytvořit GET a POST requesty pro manipulaci s dynamickými
daty na stránce, chtěl jsem k tomu využít node js na straně serveru a mongoDB jako databázi. Nicméně se objevily
problémy s hostingem a jelikož je předmět zaměřen stejně na klientskou část, tak jsem se rozhodl, že nasimuluji
čtení dat z prostých json souborů v adresáři a post zatím nebudu řešit. V sekci fotogalerie, jsem připravil
pro budoucí nahrávání fotek Drag'n'Drop funkcionalitu, kde se fotky před odesláním ukáží jako miniatury.

Tato verze rozhodně není finální, na webu je ještě spousty práce, jak úpravy funkcí, aby fungovaly ve všech prohlížečích,
ale hlavně co se týče administrace, aby byla co nejvíce pohodlná a nevyžadovala téměř žádné odborné znalosti.

Díky práci jsem se naučil dělat responsivní web a pracovat s JSON daty skrze XMLHttpRequest. Také jsem si zopakoval práci
s HTML 5 CSS 3 a Javascriptem ES 2015+.
