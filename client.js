//This class contains menu items from json file.
class menuItems {
    constructor (jsonmenu) {
        this._htmlMenu = new Map();
        for (const [name, items] of Object.entries(jsonmenu)) {
            const container = document.createElement('div');
            container.classList.add('rollMenu');
            for (let j = 0; j < items.length; j++) {
                const button = document.createElement('a');
                button.classList.add('rollItem');
                button.textContent = items[j];
                button.addEventListener("click", ()=>{
                    localStorage.setItem("menu_object", name);
                    localStorage.setItem("menu_order", j);
                })
                button.href = "page.html";
                container.appendChild(button);
            }
            this._htmlMenu.set(name, container);
        }
    }
    getMenuItems (){
        return this._menuItems;
    }
    getHtmlMenu(){
        return this._htmlMenu;
    }
}
//This function is called on load and everytime on window resize, it resize church image and facebook icon.
function resizer(reduction) {
    for (let i = 0; i < 5; i++) {
        const w = document.documentElement.clientWidth;
        document.getElementById("image-show").style.width = w+"px";
        document.getElementById("image-show").style.height = ((w/3.67333333)/reduction)+"px";
    }
    const fbEl = document.querySelector('#fb_redirect');
    fbEl.style.top = document.querySelector('header').clientHeight + "px";
}
//init function called on page load. It set up date, both calendars from web APIs, menu and events from JSON files.
function init(){
    const dateEl = document.querySelector('#datum');
    const months = ["ledna", "února", "března", "dubna", "května", "června", "července", "srpna", "září", "října", "listopadu", "prosince"];
    const date = new Date();
    dateEl.textContent = "Dnešní datum: " + date.getDate() + ". " + months[date.getMonth()] + " " + date.getFullYear();

    myRequest("http://calapi.inadiutorium.cz/api/v0/cs/calendars/czech/today")
    .then(e => {
        const data = e.target.responseText;
        const dataJson = JSON.parse(data);
        renderLiturgicCalendar(dataJson);
    })
    .catch(e => {
        console.log(e);
    });

    myRequest("http://svatky.adresa.info/json")
    .then(e => {
        const data = e.target.responseText;
        const dataJson = JSON.parse(data);
        renderSecularCalendar(dataJson);
    })
    .catch(e => {
        console.log(e);
    });
    var titulEl = document.querySelector('#titul')
    titulEl.addEventListener("click", ()=>{
        window.location.href = "index.html";
    });
    addPageListener("#svatosti");
    myRequest("menu.json")
    .then(e => {
        const data = e.target.responseText;
        const jsonmenu = JSON.parse(data);
        addMenuListeners(jsonmenu);
    })
    .catch(e => {
        console.log(e);
    });

    myRequest("events.json")
    .then(e => {
        const data = e.target.responseText;
        const dataJson = JSON.parse(data);
        renderEvents(dataJson, dataJson.pozvanky.length);
        renderNews(dataJson, dataJson.aktuality.length);
    })
    .catch(e => {
        console.log(e);
    });      
}

//This function adds event Listener to all menu items in navigation bar. Function onClick roll up menu with specific content and offClick allows click whenever in document to close this menu. 
function addMenuListeners(jsonmenu){         
    let items = document.getElementsByClassName('menu');
    for (let item of items) {
        item.addEventListener("click", function onClick(e){
            const mItems = new menuItems(jsonmenu);
            const pageEl = document.querySelector('.page')
            let container = mItems.getHtmlMenu().get(e.target.id);
            container.style.width = e.target.style.width;
            container.style.left = e.target.offsetLeft + "px";
            container.style.top = (e.target.offsetTop + e.target.offsetHeight) + "px";
            const childrens = container.children;
            for (let i = 0; i < childrens.length; i++) {
                childrens[i].style.width = e.target.clientWidth + "px";
                childrens[i].style.fontSize = getComputedStyle(e.target).fontSize;
            }
            const offClick = (evt) => {
                if(e != evt && evt.target.className != "rollItem"){
                    pageEl.removeChild(container);
                    document.removeEventListener('click', offClick);
                    for (let it of items) {
                        it.addEventListener('click', onClick);
                    }

                }

            }
            document.addEventListener('click', offClick);
            for (let it of items) {
                it.removeEventListener('click', onClick); 
            }
            pageEl.appendChild(container);
        });
    }
}
//this function render Events on page, in dependency of how far user clicked.
function renderEvents(data, length){
    for (var i = length-1; i > length-4 && i >= 0; i--) {
        const pozvanka = document.createElement('div');
        pozvanka.classList.add('pozvanka');



        const round_image = document.createElement('img');
        round_image.classList.add('round_image');
        round_image.src = data.pozvanky[i].foto_url;
        round_image.alt = data.pozvanky[i].foto_url;

        const round_image_wrap = document.createElement('div');
        round_image_wrap.classList.add('round_image_wrap');
        round_image_wrap.appendChild(round_image);

        const text_pozvanka = document.createElement('div');
        text_pozvanka.classList.add('text_pozvanka'); 

        const datum = document.createElement('span');
        datum.textContent = data.pozvanky[i].datum;
        const text = document.createElement('span');
        text.textContent = data.pozvanky[i].nazev;
        const misto = document.createElement('span');
        misto.textContent = data.pozvanky[i].misto;

        text_pozvanka.appendChild(datum);
        text_pozvanka.appendChild(text);
        text_pozvanka.appendChild(misto);
        pozvanka.appendChild(round_image_wrap);
        pozvanka.appendChild(text_pozvanka);


        document.querySelector('#pozvanky').appendChild(pozvanka);

    }
    if(i != -1){
        const next = document.createElement('a');
        next.textContent = "Další pozvánky";
        next.style.cursor = "pointer";
        const pozvanky = document.querySelector('#pozvanky');
        pozvanky.appendChild(next);
        next.addEventListener("click",()=>{
            while (pozvanky.firstChild) {
                pozvanky.removeChild(pozvanky.firstChild);
            }
            renderEvents(data, i+1);
        })
    }
    if(i < data.pozvanky.length-4){
        const back = document.createElement('a');
        back.textContent = "Předchozí pozvánky";
        back.style.cursor = "pointer";
        const pozvanky = document.querySelector('#pozvanky');
        pozvanky.insertBefore(back, pozvanky.childNodes[0]);
        back.addEventListener("click",()=>{          
            while (pozvanky.firstChild) {
                pozvanky.removeChild(pozvanky.firstChild);
            }
            renderEvents(data,3 + length);
        })
    }
}
//this function render News on page, in dependency of how far user clicked.
function renderNews(data, length){
    for (var i = length-1; i > length-6 && i >= 0; i--) {
        const aktualita = document.createElement('div');
        aktualita.classList.add('aktualita');

        const text = document.createElement('span');
        text.textContent = data.aktuality[i];

        aktualita.appendChild(text);

        document.querySelector('#aktuality').appendChild(aktualita);

    }
    if(i != -1){
        const next = document.createElement('a');
        next.textContent = "Další aktuality";
        next.style.cursor = "pointer";
        const aktuality = document.querySelector('#aktuality');
        aktuality.appendChild(next);
        next.addEventListener("click",()=>{
            while (aktuality.firstChild) {
                aktuality.removeChild(aktuality.firstChild);
            }
            renderNews(data, i+1);
        })
    }
    if(i < data.aktuality.length-6){
        const back = document.createElement('a');
        back.textContent = "Předchozí aktuality";
        back.style.cursor = "pointer";
        const aktuality = document.querySelector('#aktuality');
        aktuality.insertBefore(back, aktuality.childNodes[0]);
        back.addEventListener("click",()=>{          
            while (aktuality.firstChild) {
                aktuality.removeChild(aktuality.firstChild);
            }
            renderNews(data,5 + length);
        })
    }
}
//This function render liturgic calendar from JSON data.
function renderLiturgicCalendar(data) {
    const colorTransfer = {
        red: "Červená",
        white: "Bílá",
        pink: "Růžová",
        green: "Zelená",
        black: "Černá",
        violet: "Fialová"
    };
    const liturgic_cEl = document.querySelector("#liturgic_c");
    liturgic_cEl.textContent = "Liturgický kalendář: " + data.celebrations[0].title;
    const colorEl = document.querySelector("#color");
    colorEl.style.color = data.celebrations[0].colour;
    if(data.celebrations[0].colour == "white"){
        document.querySelector("#left_aside > span").style.backgroundColor = "rgba(72, 66, 25,0.7)";
    }
    colorEl.textContent = colorTransfer[data.celebrations[0].colour];
}
//This function render secular calendar from JSON data.
function renderSecularCalendar(data) {
    const secular_cEl = document.querySelector("#secular_c");
    secular_cEl.textContent = "Světský kalendář: " + data[0].name;
}
//This is universal function to handle XMLHttpRequests, it return Promise to deal with result.
function myRequest (url) {
    return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest();
        request.addEventListener("load", e => {
            resolve(e);
        });
        request.addEventListener("error", e => {
            reject(e);
        });
        request.overrideMimeType("data");
        request.open("GET", url);
        request.send();
    });
}
//Simple function for add listener to buttons in navigation without roll up menu.
function addPageListener(query){
    document.querySelector(query).addEventListener("click", ()=>{
        localStorage.setItem("menu_object", query.slice(1));
        localStorage.setItem("menu_order", 0);
    });      
}
init();